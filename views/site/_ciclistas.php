<?php
use yii\helpers\Html;
use yii\widgets\DetailView;

// $model representa un modelo de ciclista que será proporcionado por el ListView
?>


<!--Este es el código que determina cómo se muestran los datos y qué maquetación tienen en el ListView-->
<div class="media">
    <div class="media-body">
        <h4 class="media-heading"><?= Html::encode($model->nombre) ?></h4>
        <p><?= 'Edad: ' . Html::encode($model->edad) ?></p>
        <p><?= 'Dorsal: ' . Html::encode($model->dorsal) ?></p>
        <p><?= 'Equipo: ' . Html::encode($model->nomequipo) ?></p>
    </div>
</div>

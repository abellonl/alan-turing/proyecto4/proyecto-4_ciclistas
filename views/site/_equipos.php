
<?php
    use yii\helpers\Html;
?>

<div class="body-content">
    <div class="row">
        <div class="col-sm-8 col-md-10 mb-4">
             <div>
                 <div class="tarjeta-equipo">
                        <?= Html::img('@web/images/' . $model->nomequipo . '.png') ?>
                        <?= Html::a($model->nomequipo, ['site/mostrarequipo', 'equipo' => strtolower($model->nomequipo)], ['class'=>'btn btn-outline-primary']) ?>
                 </div>
             </div>
         </div>
    </div>
</div>
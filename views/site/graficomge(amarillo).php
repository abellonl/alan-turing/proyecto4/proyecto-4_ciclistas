<?php

use yii\helpers\Html;
use yii\helpers\Json;
use dosamigos\highcharts\HighCharts;

$data = [];
foreach ($resultados as $resultado) {
    $data[] = [
        'name' => $resultado['nombre'],
        'y' => (float) $resultado['victorias'], // Cambiado a float para aceptar valores decimales
    ];
}

$chartConfig = [
    'chart' => ['type' => 'pie'],
    'title' => ['text' => 'Cantidad de maillots amarilos ganados por estos cilistas'],
    'xAxis' => ['categories' => array_column($resultados, 'nombre')],
    'yAxis' => ['title' => ['text' => 'Victorias del ciclista']],
    'series' => [['name' => 'Victorias del ciclista', 'data' => $data]],
];

echo HighCharts::widget(['clientOptions' => $chartConfig]);

echo Html::tag('div', '', ['id' => 'grafico-puerto']);

//Texto e imagenes después del gráfico
echo '<div>';
echo '<div class="d-flex align-items-center">';
echo '<div style="margin-left: 200px;">'; // Margen a la izquierda de la imagen
echo Html::img('@web/images/indurain2.jpg', ['alt' => Yii::$app->name, 'style' => 'padding-top: 20px;']); // Añadir padding-top a la imagen
echo '</div>';
echo '<div style="margin-top: 50px;">'; // Margen superior del texto
echo '<h2 style="padding-right: 25%; padding-left: 10%;">Miguel Induráin
</h2>';
echo '<h3 style="padding-right: 25%; padding-left: 10%;">Dorsal: 1  Edad: 32  Equipo: Banesto
</h3>';
echo '<p style="padding-right: 25%; padding-left: 10%;">Indurain alcanzó la fama internacional en la década de 1990, cuando dominó el Tour de Francia de una manera que pocos ciclistas habían hecho antes. Su época dorada comenzó en 1991, cuando ganó su primer Tour de Francia, convirtiéndose en el primer ciclista español en lograrlo. Lo que siguió fue una racha increíble de cinco victorias consecutivas en el Tour, de 1991 a 1995, un récord que aún no ha sido superado.</p>';
echo '</div>';
echo '</div>';
echo '</div>';


echo '<div>';
echo '<div class="d-flex align-items-center">';
echo '<div style="margin-left: 200px;">'; // Margen a la izquierda de la imagen
echo Html::img('@web/images/tomy2.jpg', ['alt' => Yii::$app->name, 'style' => 'padding-top: 20px;']); // Añadir padding-top a la imagen
echo '</div>';
echo '<div style="margin-top: 50px;">'; // Margen superior del texto
echo '<h2 style="padding-right: 25%; padding-left: 10%;">Tony Rominger
</h2>';
echo '<h3 style="padding-right: 25%; padding-left: 10%;">Edad: 30  Dorsal: 4  Equipo: Mapei-Clas
</h3>';
echo '<p style="padding-right: 25%; padding-left: 10%;">Su carrera profesional en el ciclismo comenzó en la década de 1980, y rápidamente se destacó como un escalador formidable y un contrarrelojista excepcional. Sin embargo, fue en la década de 1990 cuando Rominger alcanzó la cima de su carrera, logrando una serie de victorias impresionantes que lo catapultaron a la fama mundial.</p>';
echo '</div>';
echo '</div>';
echo '</div>';


// Renderizamos el gráfico HighCharts
$this->registerJsFile('@web/js/highcharts.js', ['depends' => [\yii\web\JqueryAsset::class]]);
$this->registerJsFile('@web/js/exporting.js', ['depends' => [\yii\web\JqueryAsset::class]]);
$this->registerJsFile('@web/js/export-data.js', ['depends' => [\yii\web\JqueryAsset::class]]);
$this->registerCssFile('@web/css/highcharts.css');


